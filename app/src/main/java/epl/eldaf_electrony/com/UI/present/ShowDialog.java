package epl.eldaf_electrony.com.UI.present;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import epl.eldaf_electrony.com.MainActivity;
import epl.eldaf_electrony.com.R;
import epl.eldaf_electrony.com.UI.Activity.AccountStatementActivity;
import epl.eldaf_electrony.com.UI.Activity.BuyCardActivity;
import epl.eldaf_electrony.com.UI.Activity.LoginActivity;
import epl.eldaf_electrony.com.UI.utils.IntentManager;

public class ShowDialog {
    LinearLayout liybiana,etsalat,madar,bared;
    TextView Owner,Visa;
    public void showDialogCard(final Activity activity){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_mob);
        liybiana=dialog.findViewById(R.id.libiana);
        etsalat=dialog.findViewById(R.id.etsalat);
        madar=dialog.findViewById(R.id.elmadar);
        bared=dialog.findViewById(R.id.bared);
        liybiana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Bundle bundle=new Bundle();
                bundle.putString("CName","ليبيانا");
                IntentManager.startNewActivity(activity, BuyCardActivity.class,bundle);
            }
        });
        etsalat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Bundle bundle=new Bundle();
                bundle.putString("CName","الاتصالات");
                IntentManager.startNewActivity(activity, BuyCardActivity.class,bundle);
            }
        });
        madar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Bundle bundle=new Bundle();
                bundle.putString("CName","المدار");
                IntentManager.startNewActivity(activity, BuyCardActivity.class,bundle);
            }
        });
        bared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Bundle bundle=new Bundle();
                bundle.putString("CName","البريد");
                IntentManager.startNewActivity(activity, BuyCardActivity.class,bundle);
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }
    public void ShowDialogAccount(final Activity activity){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_account);
        Owner=dialog.findViewById(R.id.own);
        Visa=dialog.findViewById(R.id.visa);
        Owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                IntentManager.startNewActivity(activity, AccountStatementActivity.class,null);
            }
        });
        Visa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                IntentManager.startNewActivity(activity, AccountStatementActivity.class,null);

            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }
    public void showDialogConfirmCode(final Activity activity){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_account_num);
        Button dialogButton =  dialog.findViewById(R.id.insert);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                IntentManager.startNewActivity(activity, MainActivity.class,null);

            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();

    }
}
