package epl.eldaf_electrony.com.UI.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import epl.eldaf_electrony.com.R;
import epl.eldaf_electrony.com.UI.utils.IntentManager;
import me.anwarshahriar.calligrapher.Calligrapher;

public class InternalTransferActivity extends AppCompatActivity {
      Toolbar toolbar;
      Button confirm;TextView cancel;
      EditText money;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_internal_transfer);
        initializer();
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_forward_black_24dp);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle=new Bundle();
                bundle.putString("Mountm",money.getText().toString());
                IntentManager.startNewActivity(InternalTransferActivity.this, ConfirmTransferActivity.class,bundle);
            }
        });

    }

    public void initializer(){
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/FrutigerLTArabic.ttf", true);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        confirm=findViewById(R.id.confirmation);
        cancel=findViewById(R.id.cancel);
        money=findViewById(R.id.mountmoney);
    }
}

