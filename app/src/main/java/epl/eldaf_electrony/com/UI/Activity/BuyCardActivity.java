package epl.eldaf_electrony.com.UI.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import epl.eldaf_electrony.com.R;
import me.anwarshahriar.calligrapher.Calligrapher;

public class BuyCardActivity extends AppCompatActivity {
    Toolbar toolbar;
    Bundle bundle;
    TextView nameCompany;
    String NameC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_card);
        initializer();
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_forward_black_24dp);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/FrutigerLTArabic.ttf", true);
        Intent intent = this.getIntent();
        bundle= intent.getExtras();
        NameC= bundle.getString("CName");
        nameCompany.setText(NameC);
    }
    public void initializer(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        nameCompany=findViewById(R.id.nameC);
    }

}
