package epl.eldaf_electrony.com.UI.utils;

public class Constants {

    public static final String EXTRA_BUNDLE = "extra_bundle";
    public static final String SHAREDPREF_NAME = "my_shared_pref";

    public static final String CHANGA_FONT = "fonts/Changa-Regular.ttf";

    /**
     * User class constants
     */
    public static final String KEY_USER_NAME = "key_name";
    public static final String KEY_USER_SUBJECT = "key_subject";
    public static final String KEY_PHONE_NO = "key_phone";
    public static final String KEY_LOGGED = "key_logged";
    public static final String KEY_TOKEN = "key_token";

    /**
     * Networking Constants
     */
    public static final String BASE_URL = "";
}
