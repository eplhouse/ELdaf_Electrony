package epl.eldaf_electrony.com.UI.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import android.support.v7.widget.Toolbar;
import epl.eldaf_electrony.com.R;
import me.anwarshahriar.calligrapher.Calligrapher;

public class ConfirmTransferActivity extends AppCompatActivity {
Bundle bundle;
String money;
Toolbar toolbar;
TextView moneyEdit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_transfer);
        initializer();

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_forward_black_24dp);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );

        Intent intent = this.getIntent();
        bundle= intent.getExtras();
        money= bundle.getString("Mountm");
        moneyEdit.setText(money+" دينار");
    }
    public void initializer(){
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/FrutigerLTArabic.ttf", true);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        moneyEdit=findViewById(R.id.money);
    }
}
