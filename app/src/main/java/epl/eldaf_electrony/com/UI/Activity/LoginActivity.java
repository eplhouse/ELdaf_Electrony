package epl.eldaf_electrony.com.UI.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;


import epl.eldaf_electrony.com.MainActivity;
import epl.eldaf_electrony.com.R;
import epl.eldaf_electrony.com.UI.present.ShowDialog;
import epl.eldaf_electrony.com.UI.utils.IntentManager;
import me.anwarshahriar.calligrapher.Calligrapher;

public class LoginActivity extends AppCompatActivity {
Toolbar toolbar;
android.support.design.widget.TextInputEditText Phonenum,Acountnum,Pinnum;
Button login;
ShowDialog showDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializer();
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_forward_black_24dp);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              showDialog.showDialogConfirmCode(LoginActivity.this);
            }
        });
           Phonenum.addTextChangedListener(new TextWatcher() {
               @Override
               public void beforeTextChanged(CharSequence s, int start, int count, int after) {

               }

               @Override
               public void onTextChanged(CharSequence s, int start, int before, int count) {
                   if(s!=null&&s.length()>0){
                       char firstcharacter=s.charAt(0);
                       if (firstcharacter != '9') {
                           Phonenum.setError(getText(R.string.phone_num_start));
                           Phonenum.setText("");
                       }
                   }
               }

               @Override
               public void afterTextChanged(Editable s) {

               }
           });
    }
    public void initializer(){
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/FrutigerLTArabic.ttf", true);
        toolbar = findViewById(R.id.toolbar);
        login = findViewById(R.id.btn_login);
        Phonenum=findViewById(R.id.edittext_phonenum);
        Acountnum=findViewById(R.id.edittext_account_num);
        Pinnum=findViewById(R.id.edittext_pin);
        showDialog=new ShowDialog();
    }

}
