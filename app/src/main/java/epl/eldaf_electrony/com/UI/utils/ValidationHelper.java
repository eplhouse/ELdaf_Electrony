package epl.eldaf_electrony.com.UI.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

import epl.eldaf_electrony.com.R;


public class ValidationHelper {


    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * Checks the string is a number format
     * @param mobileNumber -> takes the string to check it
     * @return
     */
    public static boolean isMobileNumber(String mobileNumber){

        if(mobileNumber.startsWith("0")||mobileNumber.startsWith("1")||mobileNumber.startsWith("2")
                ||mobileNumber.startsWith("3") ||mobileNumber.startsWith("4")||mobileNumber.startsWith("5")
                ||mobileNumber.startsWith("6") ||mobileNumber.startsWith("7")||mobileNumber.startsWith("8")
                ||mobileNumber.startsWith("9") ||mobileNumber.contains("+")){
            return true;
        }
        else
            return false;
    }

    /**
     * Check the format of the email
     * @param emailStr -> takes a string to check
     * @return
     */
    public static boolean isEmailValid(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }

    /**
     * Checks the length of the string
     * @param str -> password string
     * @return true if meets the length provide
     */
    public static boolean meetLengthValidation(String str) {
        final int length = str.length();
        if (length >= 6 && length <= 20)
            return true;
        return false;
    }

    /**
     * Checks the mobile number format
     * @param mobileNumber -> takes the mobile no.
     * @return true if meets the string is a mobile
     */
    public static boolean isMobileNumberValid(String mobileNumber) {

        if (mobileNumber.length() < 7) {
            return false;
        }
        else if(mobileNumber.length() > 11){
            return false;
        }
        else
            return true;
    }

    /**
     * Checks the edit text empty or not
     * @param editText -> edit text
     * @param context -> activity context
     * @return false if edittext has characters
     *         true if edittext is empty amd requests a focus
     */
    public static boolean checkEditText(EditText editText, Context context) {
        String temp = editText.getText().toString().trim();
        boolean result = false;

        if (!TextUtils.isEmpty(temp)) {
            result = true;
        } else {
            editText.setError(context.getString(R.string.this_field_is_required));
            editText.requestFocus();
            result = false;
        }
        return result;
    }

    /**
     * Clears the edit text
     * @param editText -> takes the desired edit text
     */
    public static void clearEditText(EditText editText)
    {
        editText.setText("");
    }


    /**
     * removes the first characters
     * @param number -> the desired string
     * @return
     */
    public static String removeFirstCharacter(String number){
        return number.substring(1);
    }



}
