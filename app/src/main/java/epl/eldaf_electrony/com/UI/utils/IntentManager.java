package epl.eldaf_electrony.com.UI.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import epl.eldaf_electrony.com.R;


public class IntentManager {

    /**
     * Starts a new Activity
     * @param host -> the current Activity
     * @param target -> The activity you want to go
     * @param bundle -> the value of the bundle to be passed
     */
    public static void startNewActivity(Activity host, Class<? extends AppCompatActivity> target, Bundle bundle) {
        Intent intent = new Intent(host, target);
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        host.startActivity(intent);
        host.overridePendingTransition(R.anim.enter_activity, R.anim.exit_activity);
    }

    /**
     * Starts a new Activity with a custom animation
     * looks like exiting
     * @param host -> the current Activity
     * @param target -> The activity you want to go
     * @param bundle -> the value of the bundle to be passed
     */
    public static void exitToNewActivity(Activity host, Class<? extends AppCompatActivity> target, Bundle bundle) {
        Intent intent = new Intent(host, target);

        host.startActivity(intent);
        host.overridePendingTransition(R.anim.rotate_right_to_left, R.anim.exit_activity);
        host.finish();
    }

}
