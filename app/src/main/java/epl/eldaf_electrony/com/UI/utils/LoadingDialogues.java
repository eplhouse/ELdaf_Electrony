package epl.eldaf_electrony.com.UI.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import epl.eldaf_electrony.com.R;

/**
 * Creates Loading and Alert Dialogues
 */

public class LoadingDialogues {
    private static AlertDialog loadingDialogue;
    private static AlertDialog errorDialogue;
    private Context context;

    /**
     * The Constructor that takes
     * @param context -> Activity Context
     */
    public LoadingDialogues(Context context) {
        this.context = context;
    }

    /**
     * Show loading dialogue
     * @param text -> Text to be shown
     * @param isCancel -> is dialogue can be cancelled or not
     */
    public void showLoadingDialogue(String text,boolean isCancel){

       /* loadingDialogue = new SpotsDialog.Builder().setContext(context)
                .setMessage(text+"...")
                .setCancelable(isCancel)
                .build();
        loadingDialogue.show();*/
    }

    /**
     * Removes the loading dialogue
     */
    public void removeLoadingDialogue(){
        loadingDialogue.dismiss();
    }

    /**
     * Shows the Error dialogue to the user
     * @param title -> Title to be shown
     * @param message -> The content of the dialogue
     * @param exit -> if exit =  true it will closes the current Activity
     */
    public void showErrorDialogue(String title, String message, final boolean exit){
        errorDialogue= new AlertDialog.Builder(context).create();
        errorDialogue.setTitle(title);
        errorDialogue.setMessage(message);
        errorDialogue.setIcon(R.drawable.snack_error);
        errorDialogue.setButton(context.getText(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if(exit){
                    ((Activity) context).finish();
                }else {
                    errorDialogue.dismiss();
                }
            }
        });
        errorDialogue.show();
    }

    /**
     * @return -> returns true if the loading
     * dailoge is shown
     */
    public boolean isLoadingShown(){
        return loadingDialogue.isShowing();
    }
}
