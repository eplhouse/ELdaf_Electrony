package epl.eldaf_electrony.com.UI.Activity;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import epl.eldaf_electrony.com.R;
import me.anwarshahriar.calligrapher.Calligrapher;

public class AccountStatementActivity extends AppCompatActivity {
    TextView Datefrom,Dateto;
    Toolbar toolbar;
    Calendar myCalendar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_statement);
        initializer();
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/FrutigerLTArabic.ttf", true);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_forward_black_24dp);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );
        final DatePickerDialog.OnDateSetListener date_from = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };
        final DatePickerDialog.OnDateSetListener date_to = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel_to();
            }

        };

        Datefrom.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                DatePickerDialog datePickerDialog=   new DatePickerDialog(AccountStatementActivity.this, date_from, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                long currentTime = new Date().getTime();
                datePickerDialog.getDatePicker().setMaxDate(currentTime);
                datePickerDialog.show();

            }
        });
        Dateto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                DatePickerDialog datePickerDialog=   new DatePickerDialog(AccountStatementActivity.this, date_to, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                long currentTime = new Date().getTime();
                datePickerDialog.getDatePicker().setMaxDate(currentTime);
                datePickerDialog.show();
            }
        });
    }
    public void initializer(){
        Datefrom=findViewById(R.id.from);
        Dateto=findViewById(R.id.to);
        myCalendar = Calendar.getInstance();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
    }
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        Datefrom.setText(sdf.format(myCalendar.getTime()));
    }
    private void updateLabel_to() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        Dateto.setText(sdf.format(myCalendar.getTime()));
    }
}
