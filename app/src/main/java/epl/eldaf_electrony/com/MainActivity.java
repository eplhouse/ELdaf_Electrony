package epl.eldaf_electrony.com;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import epl.eldaf_electrony.com.UI.Activity.InternalTransferActivity;
import epl.eldaf_electrony.com.UI.present.ShowDialog;
import epl.eldaf_electrony.com.UI.utils.IntentManager;
import me.anwarshahriar.calligrapher.Calligrapher;

public class MainActivity extends AppCompatActivity {
 Button ITransfer,BuyCard,AccountS;
 Toolbar toolbar;
 ShowDialog showDialog;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializer();
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_forward_black_24dp);
        toolbar.setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );
        ITransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentManager.startNewActivity(MainActivity.this, InternalTransferActivity.class,null);
            }
        });
        BuyCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showDialog.showDialogCard(MainActivity.this);
            }
        });
        AccountS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               showDialog.ShowDialogAccount(MainActivity.this);
            }
        });
    }
    public void initializer(){
        Calligrapher calligrapher = new Calligrapher(this);
        calligrapher.setFont(this, "fonts/FrutigerLTArabic.ttf", true);
        ITransfer=findViewById(R.id.Internal_transfer);
        BuyCard=findViewById(R.id.Buy_cards);
        AccountS=findViewById(R.id.Account_statement);
        toolbar = findViewById(R.id.toolbar);
        showDialog =new ShowDialog();
    }


}
